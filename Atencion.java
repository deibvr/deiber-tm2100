import javax.swing.JOptionPane;

   public class Atencion {
   
   public static void main(String arg[]){
   
   //Se definen las variables 
   int identificacion, edad, opcion;
   String nombre, datoLeido;
   double peso, altura;
   edad=0;
   //Fin de la deficinicion de variables
   
   //Referencia a paciente en null
   Paciente paciente=null;
   
   
   //Menu
   do { 
        datoLeido=JOptionPane.showInputDialog("******** Menu IMC********"
                                            +"\n1. Ingresar datos del paciente"
                                            +"\n2. Modificar la altura del paciente"
                                            +"\n3. Mofificar el peso del paciente"
                                            +"\n4. Es el paciente mayor de edad?"
                                            +"\n5. Calcular el imc del paciente"
                                            +"\n6. Mostrar los datos"
                                            +"\n7. Salir");
   
    opcion=Integer.parseInt(datoLeido);
    
    //Menu switch que mantiene al usuario en el menu hasta que seleccione la opcion 7
   switch(opcion){
	   //Opcion que comprueba que se ha creado una instancia a paciente, y si no se ha credo solicita datos y la crea
	    case 1:
	      if (paciente==null){
	    datoLeido=JOptionPane.showInputDialog( "Digite el numero de identificacion  del paciente");
		identificacion=Integer.parseInt(datoLeido); 
		nombre=JOptionPane.showInputDialog( "Digite nombre del paciente a calcular");
		datoLeido=JOptionPane.showInputDialog( "Digite la edad del paciente");
		edad=Integer.parseInt(datoLeido);
		datoLeido=JOptionPane.showInputDialog( "Digite el peso del paciente en kg");
		peso=Double.parseDouble(datoLeido);
		datoLeido=JOptionPane.showInputDialog( "Digite la altura del paciente en m");
		altura=Double.parseDouble(datoLeido); 
		paciente= new Paciente( identificacion,  nombre,  edad,  peso, altura);
		JOptionPane.showMessageDialog(null, "Se ha registrado el paciente exitosamente");
		
	   }else{
		   JOptionPane.showMessageDialog(null, "El paciente ya se encuentra inscrito");
	   }
       break;
       //Fin del case 1
	    
	   //Opcion que verifica que ya este creada la instancia paciente y permite modificar la altura 
	    case 2:
	    if (paciente!=null) {
		
		datoLeido=JOptionPane.showInputDialog( "Digite el nuevo dato de altura del paciente en m");
		altura=Double.parseDouble(datoLeido); 
		paciente.setAltura(altura);
		}else{
		   JOptionPane.showMessageDialog(null, "No se ha inscrito ningun paciente");
	   }
	    break;
	    //Fin del case 2 para modificar la altura
	    
	    //Opcion que verifica que ya este creada la instancia paciente y permite modificar el peso
	    case 3:
	    if (paciente!=null) {
			
		datoLeido=JOptionPane.showInputDialog( "Digite el nuevo dato de peso del paciente en kg");
		peso=Double.parseDouble(datoLeido); 
		paciente.setPeso(peso);
		}else{
		   JOptionPane.showMessageDialog(null, "No se ha inscrito ningun paciente");
	   }
	    break;
	    //Fin del case 2 para modificar la altura
	    
	    //Opcion que verifica que ya este creada la instancia paciente y  retorna verdadero 
	    case 4:
	    if (paciente!=null) {
		JOptionPane.showMessageDialog(null, "La edad del paciente es: " +edad +  " y por lo tanto es: " + paciente.retornarVerdadero());
		
	    }else{
		   JOptionPane.showMessageDialog(null, "No se ha inscrito ningun paciente");
	   }
	    break;
	    //Fin del case 4
	    
	    //Opcion que verifica que ya este creada la instancia paciente, y hace el llamado al metodo para calcular el imc
	    case 5:
	    if (paciente!=null) {
			JOptionPane.showMessageDialog(null,"El indice de masa corporal es " +paciente.calcularIMC());
			}else{
		   JOptionPane.showMessageDialog(null, "No se ha inscrito ningun paciente");
	  }
	    break;
	    //Fin del case 5
	    
	    
	    //Opcion que verifica que ya este creada la instancia paciente, y muestra los datos del paciente
	    case 6:
	    if (paciente!=null) {
			JOptionPane.showMessageDialog(null,paciente.toString());
			}else{
		   JOptionPane.showMessageDialog(null, "No se ha inscrito ningun paciente");
	  }
	    break;
	    //Fin del case 6
	    
	    //Opcion para salir
	    case 7:
	    JOptionPane.showMessageDialog(null,"Fin de calculo de imc");
          break;
	    //Fin del case 7
	    
       default: JOptionPane.showMessageDialog(null,"Esa opción no se encuentra disponible");
       
       }//Fin del switch
    }while(opcion!=7); //Fin del menú Principal
  
   
   
   
   
   
   
   
   
   
   
   }//Fin del main
   }// Fin de la clase Atencion
