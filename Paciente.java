
public class Paciente {

//Se dcclran las variables
private int identificacion;
private String nombre;
private int edad;
private double peso;
private double altura;
// Fin de la declaracion de variables 

//Metodo constructor sin parametros que asigne los valores por omisiòn a cada atributo
    public Paciente () {
	identificacion=0;
	nombre=" ";
	edad=0;
	peso=0.0;
	altura=0.0;
	
}//Fin del constructor sin parametros

//Metodo constructor que recibe todo los valores por parametro y le asigne los atributos
    public Paciente (int identificacion, String nombre, int edad, double peso,double altura){
	this.identificacion=identificacion;
	this.nombre=nombre;
	this.edad=edad;
	this.peso=peso;
	this.altura=altura;
	
}//Fin del constructor con parametros

//Metodos set y get del atributo identificacion
       public void setIdentificacion( int identificacion) {
		   this.identificacion=identificacion;
	   }
	   public int getIdentificacion(){
		   return identificacion;
	   }
	   // Fin del metodo set y get del Identificacion
	   
//Metodos set y get del atributo nombre
       public void setNombre( String nombre) {
		   this.nombre=nombre;
	   }
	   public String getNombre(){
		   return nombre;
	   }
	   // Fin del metodo set y get del nombre

//Metodos set y get del atributo edad
       public void setEdad( int edad) {
		   this.edad=edad;
	   }
	   public int getEdad(){
		   return edad;
	   }
	   // Fin del metodo set y get del edad

//Metodos set y get del atributo peso
       public void setPeso( double peso) {
		   this.peso=peso;
	   }
	   public double getPeso(){
		   return peso;
	   }
	   // Fin del metodo set y get del peso

//Metodos set y get del atributo altura
       public void setAltura( double altura) {
		   this.altura=altura;
	   }
	   public double getAltura(){
		   return altura;
	   }
	   // Fin del metodo set y get del altura

//Metodo to String que retorna los datos del objeto
      public String toString(){
		return        "Identificacion : " +identificacion+
	                  " Nombre : " +nombre+
	                  " Edad : " +edad+
	                  " Peso: " +peso+
	                  " Altura: " +altura;  	
      }//Fin del toString
      
//Metodo que retorna verdadero si la persona es mayor de edad
      public String retornarVerdadero () {
		  
		if (edad>=18) {
		return "  Verdadero ";
		 
	    }else{ 
		 
		return " Falso ";
		   
		}// Fin del else
		
	}//Fin del metodo retornarVerdadero
	
//Metodo para calcular el imc del paciente
       public String calcularIMC () {
       double imc;
       imc= peso/(altura*2);
       if (imc<16){
		   return + imc+ "Criterio de ingreso en el hospital";
		}else{
			}
			if (imc<=17){
	      return + imc + " Infrapeso";
	     }else{
			}
			if (imc<=18){
				return + imc + " Bajo Peso";
	      }else{
		  }
			  if (imc<=25){
				return + imc + " Peso Normal (saludable)";
	      }else{
		  }
		       if (imc<=30){
				return + imc + " Sobrepeso (obesidad grado I)";
	    }else{
		} 
              if (imc<=35){
				return + imc + " Sobrepeso cronico (obesidad grado II)";
	      }else{
		  }
			if (imc<=40){
				return + imc + " Obesidad pre morbida (obesidad grado III";
	      }else{  

				return + imc + " Obesidad morbida (obesidad grado IV)";
	      }// Fin del else


          }// fin del metodo para calcular el imc
          
          












}// Fin de la clase paciente

